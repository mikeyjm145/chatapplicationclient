/**
 *
 */
package edu.westga.ChatClient.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import edu.westga.ChatClient.model.ChatConnection;
import edu.westga.ChatClient.model.MessageReceiver;

/**
 * Starts a chat client that sends an receives messages.
 *
 * @author Michael Morguarge
 *
 * @version Apr 10, 2016
 */
public class ChatClientController {

	private static final String HOST = "Localhost";
	private static final int PORT = 4321;

	/**
	 * Starts the chat server.
	 *
	 * @precondition None
	 * @postcondition None
	 *
	 * @param args Not used
	 */
	public static void main(String[] args) {
		try {
			ChatConnection chat = new ChatConnection(HOST, PORT);
			chat.connect();
			MessageReceiver messageDisplayer = new MessageReceiver(chat.getSocket());
			messageDisplayer.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			try {
				System.out.print("Your name (no spaces): ");
				String setname = "NAME " + reader.readLine();
				boolean isWhitespace = setname.split(" ", 5)[1].matches("^\\s*$");
				chat.send(setname);
				
				if (setname.length() > 5 && !isWhitespace) {
					String message = "";
					while(!message.equalsIgnoreCase("quit") && messageDisplayer.isWorking() && !chat.getSocket().isClosed()) {
						message = reader.readLine();
						chat.send(message);
					}
				}
				
				chat.send("quit");
				messageDisplayer.stopSearching();
				System.out.println("Goodbye!");
			} catch (IOException e) {
				System.err.println("Something went wrong. Please try again later.");
				e.printStackTrace();
			}
		} catch (UnknownHostException e) {
			System.err.println("Something went wrong. Please try again later.");
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Something went wrong. Please try again later.");
			e.printStackTrace();
		}
	}

}
