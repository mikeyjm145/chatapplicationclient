/**
 *
 */
package edu.westga.ChatClient.model;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Establishes a connection to a host and port number.
 *
 * @author Michael Morguarge
 *
 * @version Apr 10, 2016
 */
public class ChatConnection {

	private String host;
	private int portNum;
	private Socket socket;
	private PrintWriter out;

	/**
	 * Creates a connection between a chat server and a client.
	 *
	 * @precondition host cannot be null and portNum must be greater than zero.
	 * @postcondition host and port number are set.
	 *
	 * @param host The ip or host name of network.
	 * @param portNum The port number on that specified network to search.
	 */
	public ChatConnection(String host, int portNum) {
		if (host == null) {
			throw new IllegalArgumentException("Host is null.");
		}

		if (portNum < 0) {
			throw new IllegalArgumentException("Invalid port number.");
		}

		this.host = host;
		this.portNum = portNum;
		this.socket = null;
	}

	/**
	 * Attempts to connect to a server.
	 * @throws IOException
	 * @throws UnknownHostException
	 *
	 * @precondition None
	 * @postcondition Client connects to the server on specified port.
	 */
	public void connect() throws UnknownHostException, IOException {
		this.socket = new Socket(this.host, this.portNum);

		if (this.socket.isBound()) {
			this.out = new PrintWriter(this.socket.getOutputStream(), true);
			System.err.println("You have successfully connected to the server.");
		} else {
			System.err.println("Could not connect to server. Please try again later.");
		}
	}

	/**
	 * Sends a message from the client.
	 *
	 * @precondition message cannot be null.
	 * @postcondition The message is sent.
	 *
	 * @param message The message to send.
	 * 
	 * @return Whether the message sent or not.
	 */
	public boolean send(String message) {
		boolean sent = false;
		if (message == null) {
			throw new IllegalArgumentException("Message is null.");
		} else if(message.length() == 0) {
			System.out.println("<= The message was not sent. Please try again. =>");
		} else {
			this.out.println(message);
			sent = true;
		}
		return sent;
	}

	/**
	 * Returns the socket connected to the server.
	 *
	 * @precondition None
	 * @postcondition None
	 *
	 * @return The connected socket.
	 */
	public Socket getSocket() {
		return this.socket;
	}

}
