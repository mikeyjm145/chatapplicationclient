/**
 *
 */
package edu.westga.ChatClient.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Generates a Message Receiver that receives messages.
 *
 * @author Michael Morguarge
 *
 * @version Apr 10, 2016
 */
public class MessageReceiver extends Thread {

	private Socket socket;
	private boolean keepWorking;

	/**
	 * Create a Message Receiver to check for received messages.
	 *
	 * @precondition socket cannot be null.
	 * @postcondition The socket is set and keep working is set to true.
	 *
	 * @param socket The socket connected to the server.
	 */
	public MessageReceiver(Socket socket) {
		if (socket == null) {
			throw new IllegalArgumentException("Socket connection is null.");
		}

		this.socket = socket;
		this.keepWorking = true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		BufferedReader in = null;

		try {
			in = new BufferedReader(
					new InputStreamReader(this.socket.getInputStream()));

			while (this.keepWorking && !this.socket.isClosed()) {
				if (in.ready()) {
					String message = in.readLine();
					if (message != null && !message.equals("null")) {
						System.out.println(message);
					}
				}
			}
		} catch (IOException e) {
			System.err.println("Something went wrong. Please try again later.");
			this.stopSearching();
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
					this.socket.close();
					this.stopSearching();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Stops the execution of searching for messages.
	 *
	 * @precondition None
	 * @postcondition keep working is set to false;
	 */
	public void stopSearching() {
		this.keepWorking = false;
	}

	/**
	 *
	 *
	 * @postcondition
	 * @return
	 */
	public boolean isWorking() {
		return this.keepWorking;
	}

}
